<?php
namespace App\Modules\Feed\Interfaces;

interface FeedServiceInterface
{
    public function pullData(string $url):array;
    public function buildData(array $data):array;
}
