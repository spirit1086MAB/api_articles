<?php

namespace App\Modules\Feed\Services;

use App\Modules\Feed\Interfaces\FeedServiceInterface;

class FeedService extends Api implements FeedServiceInterface
{
    final public function pullData(string $url):array
    {
        $feed = $this->get($url);
        return $this->getResponse($feed);
    }

    final public function buildData(array $items):array
    {
        $article=[];
        foreach($items as $item)
        {
            $article[] = [
              'title'=>$item['title'],
              'slug'=>$item['slug'],
              'content'=>isset($item['content'][0]['content']) ? $item['content'][0]['content'] : null,
              'status'=>$item['properties']['status'],
              'published'=>$item['properties']['published'],
              'modified'=>$item['properties']['modified'],
              'categories'=>$item['categories'],
              'media'=>isset($item['media']) ? $item['media'] : null
            ];
        }
        return $article;
    }
}
