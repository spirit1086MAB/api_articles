<?php

namespace App\Modules\Feed\Services;

use App\Modules\Feed\Errors\ApiResponseFail;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

abstract class Api
{
    private $feed;

    public function __construct()
    {
        $this->feed = new Client(['headers' => ['Content-Type' => 'application/json']]);
    }

    public function get(string $url,array $options=[]):ResponseInterface
    {
        return $this->feed->get($url,$options);
    }

    public function post(string $url,array $options=[]):ResponseInterface
    {
        return $this->feed->post($url,$options);
    }

    public function put(string $url,array $options=[]):ResponseInterface
    {
        return $this->feed->put($url,$options);
    }

    public function patch(string $url,array $options=[]):ResponseInterface
    {
        return $this->feed->patch($url,$options);
    }

    public function delete(string $url,array $options=[]):ResponseInterface
    {
        return $this->feed->delete($url,$options);
    }

    public function getResponse(ResponseInterface $response)
    {
        $statusCode = $this->getStatusCode($response);
        if($statusCode!=200)
        {
            throw new ApiResponseFail(__('Feed::main.response_error_code').' '.$statusCode);
        }
        $content = $this->getData($response);
        return $this->decodeData($content);
    }

    private function getData(ResponseInterface $response):string
    {
        return $response->getBody()->getContents();
    }

    private function decodeData(string $data):array
    {
        return json_decode($data, true);
    }

    private function getStatusCode(ResponseInterface $response)
    {
        return $response->getStatusCode();
    }

}
