<?php
namespace App\Modules\Feed\Errors;

class ApiResponseFail extends \Exception
{
    function __construct(string $message)
    {
        $this->message = $message;
    }
}
