<?php

namespace App\Modules\Article\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Article extends Model
{
    use HasFactory;

     protected $fillable = [
         'title',
         'slug',
         'content',
         'status',
         'published',
         'modified'
     ];

     public function primaryCategory()
     {
         return $this->hasOne(Category::class)->where('type','=','primary');
     }

    public function additionalCategories()
    {
        return $this->hasMany(Category::class)->where('type','=','additional');
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function media()
    {
        return $this->hasMany(Media::class);
    }

    public function createItem()
    {
       $this->save();
       return $this;
    }

    public function setCategoriesRelation(array $categories)
    {
       $this->categories()->saveMany($categories);
       return $this;
    }

    public function setMediaRelation(array $media)
    {
        $this->media()->saveMany($media);
        return $this;
    }

    public function getAll(array $params=[])
    {
        $article = $this->join('categories', 'articles.id', '=', 'categories.article_id')
                        ->whereNotNull('categories.main')
                        ->join('media', 'articles.id', '=', 'media.article_id');

        if(isset($params['category']))
        {
            $article = $article->where('categories.title','=',$params['category']);
        }

        if(isset($params['text']))
        {
            $article = $article->where('articles.title','like','%'.$params['text'].'%')
                               ->orWhere('articles.content','like','%'.$params['text'].'%');
        }

        return $article->select('articles.*',DB::raw('categories.title as category_title'),DB::raw('media.image_name'))
                       ->paginate();
    }
}
