<?php

namespace App\Modules\Article\Models;

use App\Modules\Article\Models\Article;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    public $timestamps =false;

    protected $fillable = [
        'type',
        'media_id',
        'source',
        'slug',
        'image_name',
        'article_id'
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
