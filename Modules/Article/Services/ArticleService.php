<?php
namespace App\Modules\Article\Services;

use App\Helpers\Upload\Upload;
use App\Modules\Article\Models\{Article, Category, Media};
use App\Modules\Article\Dto\ArticleDto;
use App\Modules\Article\Interfaces\{ArticleRepositoryInterface,ArticleServiceInterface};

class ArticleService implements ArticleServiceInterface
{
    private $feedRepository;

    public function __construct(ArticleRepositoryInterface $feedRepository)
    {
        $this->feedRepository = $feedRepository;
    }

    public function fillArticles(array $items)
    {
        foreach($items as $item)
        {
            $article_dto = ArticleDto::buildFromArray($item);
            $this->addArticle($article_dto);
        }
    }

    private function addArticle(ArticleDto $article_dto)
    {
        $article_new = new Article(['title'=>$article_dto->title,'slug'=>$article_dto->slug,'content'=>$article_dto->content,'status'=>$article_dto->status,'published'=>$article_dto->published,'modified'=>$article_dto->modified]);
        $article = $this->feedRepository->setArticle($article_new);

        $categories = $this->categoryRelation($article_dto->categories);
        $media = $this->mediaRelation($article_dto->media);

        $article = $this->feedRepository->setArticleCategoriesRelation($article,$categories);
        $article = $this->feedRepository->setArticleMediaRelation($article,$media);
        return $article;
    }

    private function categoryRelation(array $items)
    {
       $categories=[];
       foreach($items as $item)
       {
           $categories[] = new Category(['title'=>$item['title'],'main'=>$item['main'],'type'=>$item['type'],'order'=>$item['order']]);
       }
       return $categories;
    }

    private function mediaRelation(array $items)
    {
        $media = [];
        foreach($items as $item)
        {
            $image_name = $item['image_name'];
            $remoute_image_url = $item['image_url'];
            $is_upload = Upload::file($remoute_image_url,$image_name);
            if($is_upload)
            {
                $media[] = new Media(['type'=>$item['type'],'media_id'=>$item['media_id'],'source'=>$item['source'],'slug'=>$item['slug'],'image_name'=>$item['image_name']]);
            }
        }

        return $media;
    }
}
