<?php

namespace App\Modules\Article\Services;

use App\Modules\Article\Interfaces\ArticleApiServiceInterface;
use App\Modules\Article\Models\Article;
use App\Modules\Article\Repositories\ArticleApiRepository;

class ArticleApiService implements ArticleApiServiceInterface
{
   private $article;
   private $articleApiRepository;

   public function __construct(Article $article,ArticleApiRepository $articleApiRepository)
   {
      $this->article = $article;
      $this->articleApiRepository = $articleApiRepository;
   }

   public function findAll(array $params)
   {
      return $this->articleApiRepository->all($this->article,$params);
   }
}
