<?php

namespace App\Modules\Article\Http;

use App\Modules\Article\Models\Article;
use App\Modules\Article\Repositories\ArticleApiRepository;
use App\Modules\Article\Resources\ArticleCollection;
use App\Modules\Article\Services\ArticleApiService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    private $articleApiService;

    public function __construct()
    {
       $this->articleApiService = new ArticleApiService(new Article(),new ArticleApiRepository());
    }

    public function articles(Request $request)
    {
       $articles = $this->articleApiService->findAll($request->all());
       return new ArticleCollection($articles);
    }
}
