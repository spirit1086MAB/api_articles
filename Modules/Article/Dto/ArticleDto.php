<?php

namespace App\Modules\Article\Dto;

use Illuminate\Support\Facades\Log;

class ArticleDto
{
    public static function buildFromArray(array $response)
    {
        $article_dto = new self();
        $article_dto->title = $response['title'];
        $article_dto->slug = $response['slug'];
        $article_dto->content = $response['content'];
        $article_dto->status = $response['status'];
        $article_dto->published = $response['published'];
        $article_dto->modified = $response['modified'];
        $article_dto->categories = static::categories($response['categories']);
        $article_dto->media = static::media($response['media']);
        return $article_dto;
    }

    private static function media(array $data)
    {
       $media = [];
       if(!empty($data))
       {
           foreach($data as $item)
           {
                   $media[] = [
                       'type'=>$item['type'],
                       'media_id'=>$item['media']['id'],
                       'source'=>$item['media']['source'],
                       'slug'=>$item['media']['slug'],
                       'image_name'=>basename($item['media']['attributes']['url']),
                       'image_url'=>$item['media']['attributes']['url']
                   ];
           }
       }

       return $media;
    }

    private static function categories(array $categories)
    {
        $response=[];

        $key = $categories['primary'] ? 'primary' : (!empty($categories['additional']) ? 'additional' : null);
        $value = $categories['primary'] ? $categories['primary'] : (!empty($categories['additional']) ? $categories['additional'][0] : null);
        if($value)
        {
            $response = static::pushToArray($response,$value,$key,true);
        }

        if(isset($categories['additional']) && !empty($categories['additional']))
        {
            for($i=0;$i<count($categories['additional']);$i++)
            {
                $key = 'additional';
                $value = $categories['additional'][$i];
                $order = ($i+1);
                $response = static::pushToArray($response,$value,$key,false,$order);
            }
        }

        return $response;
    }

    private static function pushToArray(array &$response,string $title,string $type,bool $is_main=false,?string $order=null)
    {
        $response[] = [
            'title'=>$title,
            'main'=>$is_main ? 1 : null,
            'type'=>$type,
            'order'=>$order
        ];

        return $response;
    }
}
