<?php

namespace App\Modules\Article\Interfaces;

interface ArticleServiceInterface
{
  public function fillArticles(array $items);
}
