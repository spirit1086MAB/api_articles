<?php

namespace App\Modules\Article\Interfaces;

use App\Modules\Article\Models\Article;

interface ArticleApiRepositoryInterface
{
    public function all(Article $article,array $params);
}
