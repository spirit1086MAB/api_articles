<?php
namespace App\Modules\Article\Interfaces;

use App\Modules\Article\Models\Article;

interface ArticleRepositoryInterface
{
   public function setArticle(Article $article);
   public function setArticleCategoriesRelation(Article $article,array $categories);
   public function setArticleMediaRelation(Article $article,array $media);
}
