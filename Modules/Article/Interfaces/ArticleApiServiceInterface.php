<?php

namespace App\Modules\Article\Interfaces;

interface ArticleApiServiceInterface
{
    public function findAll(array $params);
}
