<?php
namespace App\Modules\Article\Repositories;

use App\Modules\Article\Models\Article;
use App\Modules\Article\Interfaces\ArticleRepositoryInterface;

class ArticleRepository implements ArticleRepositoryInterface
{
   public function setArticle(Article $article)
   {
       return $article->createItem();
   }

   public function setArticleCategoriesRelation(Article $article,array $categories)
   {
      return $article->setCategoriesRelation($categories);
   }

   public function setArticleMediaRelation(Article $article,array $media)
   {
       return $article->setMediaRelation($media);
   }
}
