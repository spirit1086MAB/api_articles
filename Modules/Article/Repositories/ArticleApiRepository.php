<?php

namespace App\Modules\Article\Repositories;

use App\Modules\Article\Interfaces\ArticleApiRepositoryInterface;
use App\Modules\Article\Models\Article;

class ArticleApiRepository implements ArticleApiRepositoryInterface
{
    public function all(Article $article,array $params)
    {
       return $article->getAll($params);
    }
}
