<?php

Route::group( [ 'namespace' => 'App\Modules\Article\Http','prefix'=> 'api','middleware' => ['api']], function()
{
    Route::get('/articles','ArticleController@articles')->name('articles');
});

