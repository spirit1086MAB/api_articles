<?php

namespace App\Console\Commands;

use App\Modules\Article\Repositories\ArticleRepository;
use App\Modules\Article\Services\ArticleService;
use App\Modules\Feed\Services\FeedService;
use Illuminate\Console\Command;

class Feed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from feed (format json)';

    private $feedService;
    private $articleService;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->feedService = new FeedService();
        $this->articleService = new ArticleService(new ArticleRepository());
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $response = $this->feedService->pullData('https://raw.githubusercontent.com/SeteMares/full-stack-test/master/feed.json');
        $items = $this->feedService->buildData($response);
        $this->articleService->fillArticles($items);
    }
}
